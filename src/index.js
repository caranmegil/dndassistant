/*

index.js - the code that starts the app up
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { RMWCProvider } from '@rmwc/provider';

import registerServiceWorker from './registerServiceWorker';
import rootReducer from './reducers';

import App from './App'

import './index.css';

const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <RMWCProvider buttonDefaultRipple={false}>
            <App />
        </RMWCProvider>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();