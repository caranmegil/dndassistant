/*

App.js - the main component
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
    useState
} from 'react';

import {
    TopAppBar,
    TopAppBarRow,
    TopAppBarSection,
    TopAppBarTitle
} from '@rmwc/top-app-bar'

import {
    TabBar,
    Tab
} from '@rmwc/tabs';

import {
    Typography
} from '@rmwc/typography';

import '@rmwc/top-app-bar/styles';
import '@rmwc/tabs/styles';
import '@rmwc/typography/styles';
import './App.css';

import AddEncButton from './components/encounterer/AddEncButton';
import EncounterGrid from './components/encounterer/EncounterGrid';
import ClearEncButton from './components/encounterer/ClearEncButton';

import AddPlayerButton from './components/initiative/AddPlayerButton';
import NewGameButton from './components/initiative/NewGameButton';
import EditPlayerDialog from './components/initiative/EditPlayerDialog';
import PlayerList from './components/initiative/PlayerList';

const App = () => {
    const [activeTab, setActiveTab] = useState(0);

    function renderTabs(activeTab) {
        switch(activeTab) {
            case 0: return (
                <div id="initiativizr" className="App-section">
                    <header>
                        <p>
                            <Typography use="headline4">Initiativizer!</Typography>
                        </p>
                        <p>
                            <Typography use="subtitle1">An initiative tracker</Typography>
                        </p>
                    </header>
                    <div className="button-bar">
                        <AddPlayerButton />
                        <NewGameButton />
                    </div>
                    <p>
                        To get started, press the "Add Player" button. Select the player to change the values.
                    </p>
                    <div className="player-list">
                        <PlayerList/>
                    </div>
                </div>
            );
            case 1: return (
                <div id="encounterer" className="App-section">
                    <header>
                        <p>
                            <Typography use="headline4">Encounterer!</Typography>
                        </p>
                        <p>
                            <Typography use="subtitle1">An encounter tracker</Typography>
                        </p>
                    </header>
                    <div className="button-bar">
                        <AddEncButton />
                        <ClearEncButton />
                    </div>
                    <EncounterGrid/>
                </div>
            );
        }
    }

    return (
        <div className="App">
            <TopAppBar style={{backgroundColor: "#007F5C"}}>
                <TopAppBarRow>
                    <TopAppBarSection alignStart>
                        <TopAppBarTitle>Dungeons and Dragons Assistant</TopAppBarTitle>
                    </TopAppBarSection>
                </TopAppBarRow>
                <TopAppBarRow>
                    <TabBar activeTabIndex={activeTab}
                            onActivate={evt => setActiveTab(evt.detail.index)}>
                        <Tab>Initiativizr</Tab>
                        <Tab>Encounterer</Tab>
                    </TabBar>
                </TopAppBarRow>
            </TopAppBar>
            {renderTabs(activeTab)}
        <EditPlayerDialog />
      </div>

    )
};

export default App;