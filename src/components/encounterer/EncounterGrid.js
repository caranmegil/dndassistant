/*

components/encounterer/EncounterGrid.js - the encounter grid
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import {
    Grid,
    GridCell
} from '@rmwc/grid';

import EncounterCard from './EncounterCard';

import '@rmwc/grid/styles';

const EncounterGrid = (props) => {
    return (
        <Grid>
            {props.encounters.map( encounter => {
                return <GridCell key={encounter.id}><EncounterCard {...encounter}/></GridCell>
            })}
        </Grid>
    )
}

const mapStateToProps = (state, props) => {
    const {
        encounters,
    } = state;

    return {
        encounters,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
    };
};
    
export default connect(mapStateToProps, mapDispatchToProps)(EncounterGrid);
