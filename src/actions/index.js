/*

actions/index.js - defines the actions
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
    ADD_PLAYER,
    REMOVE_PLAYER,
    NEW_GAME,
    UPDATE_PLAYER,
    EDIT_PLAYER_INFO,
    ADD_ENCOUNTER,
    REMOVE_ENCOUNTER,
    UPDATE_ENCOUNTER,
    CLEAR_ENCOUNTERS,
  } from './types';
  
  export const addPlayer = () => {
    return {
      type: ADD_PLAYER
    }
  };
  
  export const removePlayer = (id) => {
    return {
      type: REMOVE_PLAYER,
      id
    }
  };
  
  export const startNewGame = () => {
    return {
      type: NEW_GAME
    }
  };
  
  export const updatePlayer = (id, name, initiative) => {
    return {
      type: UPDATE_PLAYER,
      id,
      name,
      initiative,
    }
  };
  
  export const editPlayerInfo = (id, showEdit) => {
    return {
      type: EDIT_PLAYER_INFO,
      id,
      showEdit,
    }
  };
  
  export const addEncounter = () => {
    return {
      type: ADD_ENCOUNTER,
    }
  };

  export const removeEncounter = (id) => {
    return {
      type: REMOVE_ENCOUNTER,
      id
    }
  };

  export const clearEncounters = () => {
    return {
      type: CLEAR_ENCOUNTERS
    }
  };

  export const updateEncounter = (id, name, maxHealth, damage) => {
    return {
      type: UPDATE_ENCOUNTER,
      id,
      name,
      maxHealth,
      damage,
    }
  };