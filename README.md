# dndinitiative

The Dungeons and Dragons initiative tracker.  It is written with React, ES6, CSS, and material design.  It should run in most browsers.

## Requirements

To build or test, the following components are necessary
* Node
* Yarn
* Serve (yarn add serve)

## Installation

Just run yarn to install.  It'll take it from there.

## Local Hosting

Just run yarn start to do that.

## Build For Hosting

Just run yarn build and it'll build it all.  For deployment, copy the build path.