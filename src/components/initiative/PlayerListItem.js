/*

components/initiative/PlayerListItem.js - the view responble for showing player details
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import {
  ListItem,
  ListItemText,
  ListItemSecondaryText,
  ListItemMeta,
} from '@rmwc/list';

import {
  editPlayerInfo,
  removePlayer,
} from '../../actions';

import '@rmwc/list/styles';

const PlayerListItem = (props) => {
    function onRemovePlayer(e) {
        e.stopPropagation()
        props.removePlayer(props.player.id);
    }

    function onEditPlayerInfo(e) {
        props.editPlayerInfo(props.player.id);
    }

    return (
        <ListItem onClick={onEditPlayerInfo}>
            <div className="player-list-item">
                <ListItemText>{props.player.name}</ListItemText>
                <ListItemSecondaryText>Initiative: {props.player.initiative}</ListItemSecondaryText>
            </div>
            <ListItemMeta onClick={onRemovePlayer} icon="remove_circle"/>
        </ListItem>
    )
}

const mapStateToProps = (state, props) => {
    return {

    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        editPlayerInfo: (id) => dispatch(editPlayerInfo(id, true)),
        removePlayer: (id) => dispatch(removePlayer(id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerListItem);
  