/*

components/initiative/PlayerList.js - the list responble for showing players
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import {
    List,
    ListDivider,
  } from '@rmwc/list';
import {v4 as uuidv4} from 'uuid';

import PlayerListItem from './PlayerListItem';
  
const PlayerList = (props) => {
    let listElems = [];
    let players = [...props.players];
 
    players.sort((a,b) => {
        let retVal = 0;
        if(a.initiative > b.initiative) {
            retVal = -1;
        } else if(a.initiative < b.initiative) {
           retVal = 1;
        }
        return retVal;
    });

    const playersLen = players.length;

    for(let playerI = 0; playerI < playersLen; playerI++) {
      let player = players[playerI];
      listElems.push(<PlayerListItem player={{...player}} key={player.id}/>);
      if(playerI < (playersLen - 1)) {
        listElems.push(<ListDivider key={uuidv4()}/>);
      }
    }
    return (
      <List>
      { listElems }
      </List>
    );
}

const mapStateToProps = (state, props) => {
const {players} = state;
    return {
        players,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerList);
  