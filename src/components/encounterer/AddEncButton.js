/*

components/initiative/AddEncButton.js - the button responble for adding encounters
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import {Button} from '@rmwc/button';

import '@rmwc/button/styles';

import { addEncounter } from '../../actions'

const AddEncButton = (props) => {

    function onClick(e) {
        e.preventDefault()
        props.addEncounter();
    }

    return (
        <Button raised style={{backgroundColor: "#007F5C"}} onClick={onClick} icon="add" label="Add Encounter"/>
    )
}

const mapStateToProps = (state, props) => {
    return {
  
    };
  };
  
  const mapDispatchToProps = (dispatch, props) => {
    return {
      addEncounter: () => dispatch(addEncounter())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddEncButton);