/*

components/initiative/EditPlayerDialog.js - the dialog responble for editing players
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogButton,
//   DialogBackdrop
} from '@rmwc/dialog';

import {
  TextField,
} from '@rmwc/textfield';

import '@rmwc/dialog/styles';
import '@rmwc/textfield/styles';

import {
  editPlayerInfo,
  removePlayer,
  updatePlayer,
} from '../../actions';
import { useState } from 'react';

const EditPlayerDialog = (props) => {
    let player = props.players.filter((player) => {return props.currentEditPlayer != null && player.id === props.currentEditPlayer})[0];

    var [playerName, setPlayerName] = useState('')
    var [initiative, setInitiative] = useState(0)

   function onPlayerNameChange(e) {
        setPlayerName(e.target.value)
    }
    
    function onInitiativeChange(e) {
        setInitiative(parseInt(e.target.value, 10))
    }

    function closeDialog() {
        props.closeDialog();
    }
    
    function updatePlayer() {
        if(props.currentEditPlayer != null) {
            props.updatePlayer(props.currentEditPlayer, playerName, initiative);
            props.closeDialog();
        }
    }

    function onOpened() {
        setPlayerName(player.name)
        setInitiative(player.initiative)
    }

    function onDelete() {
        if(props.currentEditPlayer != null) {
            props.removePlayer(props.currentEditPlayer);
            props.closeDialog();
        }
    }
      
    if(player !== undefined) {
        return (
            <Dialog
                open={props.showPlayerEditor}
                onOpened={onOpened}
                onClose={closeDialog}
            >
                <DialogTitle>Edit Player</DialogTitle>
                <DialogContent>
                    <TextField label="Name" value={playerName} onChange={onPlayerNameChange} />
                    <TextField label="Initiative" value={initiative} onChange={onInitiativeChange} type="number" pattern="\d*" />
                </DialogContent>
                <DialogActions>
                    <DialogButton onClick={closeDialog}>Cancel</DialogButton>
                    <DialogButton onClick={onDelete}>Delete</DialogButton>
                    <DialogButton onClick={updatePlayer}>Save</DialogButton>
                </DialogActions>
            </Dialog>
        );
    } else {
        return <div />
    }
}

const mapStateToProps = (state, props) => {
    const {
        players,
        currentEditPlayer,
        showPlayerEditor,
    } = state;

    return {
        players,
        currentEditPlayer,
        showPlayerEditor,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        closeDialog: () => dispatch(editPlayerInfo(null,false)),
        removePlayer: (id) => dispatch(removePlayer(id)),
        updatePlayer: (id, name, initiative) => dispatch(updatePlayer(id, name, initiative)),
    };
};
    
export default connect(mapStateToProps, mapDispatchToProps)(EditPlayerDialog);
  