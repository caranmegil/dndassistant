/*

reducers.js - the reducers
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { combineReducers } from "redux";
import { v4 as uuidv4 } from 'uuid';

import {
    ADD_PLAYER,
    REMOVE_PLAYER,
    NEW_GAME,
    UPDATE_PLAYER,
    EDIT_PLAYER_INFO,
    ADD_ENCOUNTER,
    REMOVE_ENCOUNTER,
    UPDATE_ENCOUNTER,
    CLEAR_ENCOUNTERS,
  } from './actions/types';

  const currentEditPlayer = (state = null, action) => {
    switch(action.type) {
      case EDIT_PLAYER_INFO:
        return action.id;
      default:
        return state;
    }
  };
  
  const showPlayerEditor = (state = false, action) => {
    switch(action.type) {
      case EDIT_PLAYER_INFO:
        return action.showEdit;
      default:
        return state;
    }
  };
  
  const getDefaultEncounters = () => {
    let encounters = localStorage.encounters;
    if (encounters === '' || encounters === undefined || encounters == null) {
      encounters = []
      localStorage.encounters = JSON.stringify(encounters);
    } else {
      encounters = JSON.parse(encounters);
    }
    return encounters;
  }

  const getDefaultPlayers = () => {
    let players = localStorage.encounters;
    if(players === '' || players === undefined || players == null) {
      players = [];
      localStorage.players = JSON.stringify(players);
    } else {
      players = JSON.parse(players);
    }
    return players;
  }

  const encounters = (state = getDefaultEncounters(), action) => {
    switch(action.type) {
      case ADD_ENCOUNTER:
        let newEncounters = [ ...state, {
          name: 'New Encounter',
          maxHealth: 0,
          damage: 0,
          id: uuidv4()
        }];
        localStorage.encounters = JSON.stringify(newEncounters);
        return newEncounters
      case REMOVE_ENCOUNTER:
        let removedEncounters = state.filter( encounter => encounter.id !== action.id );
        localStorage.encounters = JSON.stringify(removedEncounters);
        return removedEncounters;
      case UPDATE_ENCOUNTER:
        let updatedEncounters = [...state];
        updatedEncounters.forEach( encounter => {
          if (encounter.id == action.id) {
            encounter.name = action.name;
            encounter.maxHealth = action.maxHealth;
            encounter.damage = action.damage;
          }
        });
        localStorage.encounters = JSON.stringify(updatedEncounters);
        return updatedEncounters;
      case CLEAR_ENCOUNTERS:
        localStorage.encounters = JSON.stringify([]);
        return [];
      default: return state;
    }
  }
  
  const players = (state = getDefaultPlayers(), action) => {
      switch(action.type) {
        case ADD_PLAYER:
          let newPlayers = [ ...state, {
            name: 'New Player',
            initiative: 0,
            id: uuidv4()
          }];
          localStorage.players = JSON.stringify(newPlayers);
          return newPlayers;
        case UPDATE_PLAYER:
          var updatedPlayers = [ ...state ]
          for(let playerI=0; playerI < updatedPlayers.length; playerI++) {
            if(updatedPlayers[playerI].id === action.id) {
              updatedPlayers[playerI].name = action.name
              updatedPlayers[playerI].initiative = action.initiative
            }
          };
  
          localStorage.players = JSON.stringify(updatedPlayers);
  
          return updatedPlayers;
        case REMOVE_PLAYER:
          let removedPlayers = state.filter(player => player.id !== action.id)
          localStorage.players = JSON.stringify(removedPlayers);
          return removedPlayers;
        case NEW_GAME:
          localStorage.players = JSON.stringify([])
          return [];
        default:
          return state;
      }
  };
  
  export default combineReducers(
    {
      players,
      currentEditPlayer,
      showPlayerEditor,
      encounters,
    }
  );
  