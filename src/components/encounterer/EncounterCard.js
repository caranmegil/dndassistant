/*

components/encounterer/EncounterCard.js - the card responsible for manipulating encounters
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import {
    Card,
    CardPrimaryAction,
    CardActions,
    CardActionButtons,
    CardActionButton,
} from '@rmwc/card';

import {
    TextField
} from '@rmwc/textfield'

import '@rmwc/card/styles';
import '@rmwc/textfield/styles';

import {
    removeEncounter,
    updateEncounter,
  } from '../../actions';
  import { useState } from 'react';  

const EncounterCard = (props) => {
    const [encName, setEncName] = useState(props.name);
    const [encMaxHealth, setEncMaxHealth] = useState(props.maxHealth);
    const [encDamage, setEncDamage] = useState(props.damage);

    function updateEncName(e) {
        setEncName(e.target.value)
        props.updateEncounter(props.id, e.target.value, encMaxHealth, encDamage)
    }

    function updateEncMaxHealth(e) {
        setEncMaxHealth(parseInt(e.target.value))
        props.updateEncounter(props.id, encName, parseInt(e.target.value), encDamage)
    }

    function updateEncDamage(e) {
        setEncDamage(parseInt(e.target.value))
        props.updateEncounter(props.id, encName, encMaxHealth, parseInt(e.target.value))
    }

    function removeEncounter() {
        props.removeEncounter(props.id);
    }

    return (
        <Card>
            <CardPrimaryAction>
                <TextField label="Name" value={encName} onChange={updateEncName}/>
                <TextField label="Max Health" value={encMaxHealth} onChange={updateEncMaxHealth} type="number" pattern="\d*" />
                <TextField label="Damage" value={encDamage} onChange={updateEncDamage} type="number" pattern="\d*" />
            </CardPrimaryAction>
            <CardActionButtons>
                <CardActionButton label="Remove" onClick={removeEncounter}/>
            </CardActionButtons>
        </Card>
    )
}

const mapStateToProps = (state, props) => {
    return {

    };
};
  
const mapDispatchToProps = (dispatch, props) => {
    return {
        removeEncounter: (id) => dispatch(removeEncounter(id)),
        updateEncounter: (id, name, maxHealth, damage) => dispatch(updateEncounter(id, name, maxHealth, damage)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EncounterCard);